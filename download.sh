#!/usr/bin/env bash

MAX_DL=2
WAIT_DELAY=1
FILE_URL_LIST="dl_list.txt"
DOWNLOAD_DIR="dl"

######### DO NOT EDIT FROM HERE / EDIT IT AT YOUR OWN RISK #########

# This function initializes the system prior to main processing
init () {
  set -o nounset    # Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
  ulimit -c 0       # Set Core size to 0

  # Call the function cleanupBeforeExit when the signal EXIT is catched
  trap "cleanupBeforeExit" EXIT
}

# This function is called before exiting the program
cleanupBeforeExit () {
  loginfo "Download.sh Done."
}

# Format strings for logfunctions
format () {
  echo "$(date +"%d/%m/%y %T") [${1}]";
}

loginfo ()  { echo -e "\033[32m$(format INFO)\033[0m ${@}"; }
lognotice ()  { echo -e "\033[34m$(format NOTICE)\033[0m ${@}"; }
logwarning () { echo -e "\033[33m$(format WARNING)\033[0m ${@}"; }
logerror () { echo -e "\033[31m$(format ERROR)\033[0m ${@}" 1>&2; }

# This function creates a folder
# $1: path
createNewDir () {
  if [ ! -d "${1}" ]; then
    mkdir -p ${1}
    loginfo "Folder ${1} created."
  else
    loginfo "Folder ${1} is already existing."
  fi
}

# This function checks if a file exists
# $1: path
isFilePresent () {
  if [ -f "${1}" ]; then
    loginfo "${1} exist."
    return 0;
  else
    return 1;
  fi
}

# This function download the link given in silent with wget
# $1: link
download () {
  loginfo "Downloading ${1}."
  wget -bqc ${1}
}

# This function all the files with the downloader name in it.
renameFiles () {
  loginfo "Cleaning mkv|mp4|avi files by renaming them if needed."
  rename -v "s/-([a-zA-Z0-9_.]+)\.mkv/\.mkv/" *.mkv
  rename -v "s/-([a-zA-Z0-9_.]+)\.mp4/\.mp4/" *.mp4
  rename -v "s/-([a-zA-Z0-9_.]+)\.avi/\.avi/" *.avi
}

# Overall processing
main () {
  init "$@"
  isFilePresent "${FILE_URL_LIST}"
  if [ $? == 1 ]; then
    logerror "${FILE_URL_LIST} file not found."
    exit 1;
  fi
  createNewDir "${DOWNLOAD_DIR}"

  while IFS='\n' read -r URL; do
    FILE_NAME="${URL##*/}"
    ACTU_PATH=`pwd`
    NB_DL=`ps aux | grep "wget -bqc" | grep -v "grep" | wc -l | tr -d '[[:blank:]]'`
    if [ ${ACTU_PATH##*/} != "${DOWNLOAD_DIR}" ]; then
      cd "${DOWNLOAD_DIR}"
    fi
    while [ ${NB_DL} -ge ${MAX_DL} ]; do
      loginfo "Waiting ${WAIT_DELAY}s to retry to dl ${FILE_NAME}, too much downloads at the same time (${NB_DL}/${MAX_DL})."
      sleep ${WAIT_DELAY}
      NB_DL=`ps aux | grep "wget -bqc" | grep -v "grep" | wc -l | tr -d '[[:blank:]]'`
    done
    if [ ${NB_DL} -lt ${MAX_DL} ]; then
      loginfo "Number of current downloads : ${NB_DL} < ${MAX_DL}"
      download "${URL}"
    fi
  done < "$FILE_URL_LIST"
  while [ ${NB_DL} -gt 0 ]; do
    loginfo "Waiting ${WAIT_DELAY}s to finish downloading files (${NB_DL})."
    sleep ${WAIT_DELAY}
    NB_DL=`ps aux | grep "wget -bqc" | grep -v "grep" | wc -l | tr -d '[[:blank:]]'`
  done
  # renameFiles
}

# script entry point
main "$@"
